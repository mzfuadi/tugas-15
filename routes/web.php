<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/profiles/create', 'PostController@create');
Route::post('/profiles', 'PostController@profiles');
Route::get('/profiles', 'PostController@index');
Route::get('/profiles/{id}', 'PostController@show');
Route::get('profiles/{id}/edit', 'PostController@edit');
Route::put('profiles/{id}', 'PostController@update');
Route::delete('profiles/{id}', 'PostController@destroy');