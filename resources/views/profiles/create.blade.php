@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="box box-primary">
            <div class="box-header with-border mt-3">
              <h3 class="box-title">Create Profile</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/profiles" method="POST">
                @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="nama_lengkap">Nama Lengkap</label>
                  <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" value=" {{ old('nama_lengkap', '') }} " placeholder="Enter name">
                  @error('nama_lengkap')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email" value=" {{ old('email', '') }} " placeholder="email">
                  @error('email')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="foto">Foto</label>
                  <input type="foto" class="form-control" id="foto" name="foto"  placeholder="foto">
                  @error('foto')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
              </div>
            </form>
</div>
          </div>
@endsection