@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
    <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Profiles Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            @if(session('success'))
              <div class="alert alert-success">
                {{ session('success') }}
              </div>
            @endif
              <a class="btn btn-primary mb-2" href="/profiles/create">Create New Profile</a>
              <table class="table table-bordered">
              <tbody>
              <tr>
                  <th style="width: 10px">#</th>
                  <th>Nama Lengkap</th>
                  <th>Email</th>
                  <th>Foto</th>
                  <th style="width: 40px">Action</th>
                </tr>
                @forelse($profiles as $key => $profiles)
                    <tr>
                    <td> {{ $key + 1 }} </td>
                    <td> {{ $profiles-> nama_lengkap }} </td>
                    <td> {{ $profiles-> email }} </td>
                    <td> {{ $profiles-> foto }} </td>
                    <td style="display: flex;"> 
                      <a href="/profiles/{{ $profiles->id }}" class="btn btn-info btn-sm">Show</a> 
                      <a href="/profiles/{{ $profiles->id }}/edit" class="btn btn-default btn-sm">Edit</a>
                      <form action="/profiles/{{ $profiles->id }}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                      </form>
                    </td>
                    </tr>
                  @empty
                    <tr>
                      <td colspan="5" align="center"> No Profile</td>
                    </tr>
                @endforelse
              </tbody></table>
            </div>
            <!-- /.box-body -->
            <!-- <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li>
              </ul>
            </div> -->
          </div>
    
    </div>
@endsection