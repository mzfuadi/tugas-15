<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function create() {
        return view('profiles.create');
    }
    public function profiles(Request $request) {
       // dd($request->all());
        $request->validate([
            'nama_lengkap' => 'required|unique:profiles',
            'email' => 'required|unique:profiles',
            'foto' => 'required'
        ]);
        $query = DB::table('profiles')->insert([
            "nama_lengkap" => $request["nama_lengkap"],
            "email" => $request["email"],
            "foto"=> $request["foto"]
        ]);
        
        return redirect('/profiles')->with('success', 'Profile Berhasil Disimpan!');
    }
    public function index() {
        $profiles = DB::table('profiles')->get();
        return view('profiles.index', compact('profiles'));
    }
    public function show($id) {
        $profiles = DB::table('profiles')->where('id', $id)->first();
        return view('profiles.show', compact('profiles'));
    }
    public function edit($id) {
        $profiles = DB::table('profiles')->where('id', $id)->first();
        return view('profiles.edit', compact('profiles'));
    }
    public function update($id, Request $request) {
        $request->validate([
            'nama_lengkap' => 'required|unique:profiles',
            'email' => 'required|unique:profiles',
            'foto' => 'required'
        ]);
        $query = DB::table('profiles')
                ->where('id', $id)
                ->update([
                    'nama_lengkap' => $request['nama_lengkap'],
                    'email' => $request['email'],
                    'foto' => $request['foto']
                ]);
        return redirect('/profiles')->with('success', 'Profile Berhasil Diupdate!');
    }
    public function destroy($id) {
        $query = DB::table('profiles')
                ->where('id', $id)->delete();
        return redirect('/profiles')->with('success', 'Profile Berhasil Dihapus!');
    }
}
